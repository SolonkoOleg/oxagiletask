﻿using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Services.Logging.NLogIntegration;
using Castle.Windsor;
using IoC.Business.Services;
using IoC.Business.Services.Contracts;
using System.Web.Mvc;

namespace IoC.Web.Utils
{
    public class ApplicationCastleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IActionInvoker>().ImplementedBy<CastleActionInvoker>().LifeStyle.Transient);
            container.Register(Component.For<ITestService>().ImplementedBy<TestService>().LifeStyle.PerWebRequest);
            container.Register(Classes.FromThisAssembly()
                .BasedOn<IController>()
                .LifestyleTransient());
            container.AddFacility<LoggingFacility>(f => f.LogUsing<NLogFactory>()
                    .WithConfig("NLog.config"));
        }
    }
}