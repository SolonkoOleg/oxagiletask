﻿using Castle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IoC.Web.Utils
{
    public class CastleActionInvoker : ControllerActionInvoker
    {
        public ILogger Logger { get; set; } = NullLogger.Instance;

        protected override ActionResult InvokeActionMethod(ControllerContext controllerContext, 
            ActionDescriptor actionDescriptor, 
            IDictionary<string, object> parameters)
        {
            try
            {
                Logger.DebugFormat("Controller:{0},Action:{1},Parameters:{2}", controllerContext.Controller,actionDescriptor.ActionName,string.Join(",",parameters));
                var action = base.InvokeActionMethod(controllerContext, actionDescriptor, parameters);
                return action;
            }
            catch(Exception ex)
            {
                Logger.Error("Exception :: ", ex);
                return new HttpNotFoundResult();
            }            
        }
    }
}