﻿using Castle.Windsor;
using IoC.Web.Utils;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using IoC.Business.Helpers;

namespace IoC.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer _container;

        protected void Application_Start()
        {
            InitContainer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InitContainer()
        {
            _container = new WindsorContainer();
            _container.Install(new BusinessCastleInstaller("DefaultConnection"));
            _container.Install(new ApplicationCastleInstaller());
            var castleControllerFactory = new CastleControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(castleControllerFactory);
        }

        protected void Application_End()
        {
            _container.Dispose();
        }
    }
}
