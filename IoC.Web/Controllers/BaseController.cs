﻿using System.Web.Mvc;

namespace IoC.Web.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}