﻿using Castle.Core.Logging;
using IoC.Business.Services.Contracts;
using IoC.ViewModels.Models;
using System;
using System.Web.Mvc;

namespace IoC.Web.Controllers
{
    public class TestController : BaseController
    {
        private readonly ITestService _testService;
        public ILogger Logger { get; set; } = NullLogger.Instance;

        public TestController(ITestService testService)
        {
            _testService = testService;
        }

        public override ActionResult Index()
        {
            return View(_testService.GetAllUsers());
        }

        public ActionResult Create()
        {
            UserViewModel userViewModel = new UserViewModel
            {
                Companies = _testService.GetAllCompanies()
            };
            return View(userViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserViewModel user)
        {
            if (ModelState.ContainsKey("ChosedCompany.Id"))
            {
                var existCompany = _testService.GetCompanyById(user.ChosedCompany.Id);
                if (existCompany != null)
                {
                    user.ChosedCompany = existCompany;
                    _testService.AddUser(user);
                    Logger.Info("User added to database");
                }
                else
                {
                    Logger.Error("Add user failed");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            UserViewModel userViewModel = _testService.GetUserById(id);
            userViewModel.Companies = _testService.GetAllCompanies();
            return View(userViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel user)
        {
            _testService.UpdateUser(user);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            return View(_testService.GetUserById(id));
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(UserViewModel user)
        { 
            _testService.DeleteUser(user);
            return RedirectToAction("Index");
        }
    }
}