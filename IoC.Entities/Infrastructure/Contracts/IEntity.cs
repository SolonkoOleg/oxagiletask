﻿namespace IoC.Entities.Infrastructure.Contracts
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
