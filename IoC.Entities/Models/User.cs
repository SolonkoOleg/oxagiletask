﻿using IoC.Entities.Infrastructure;
using System;

namespace IoC.Entities.Models
{
    public class User : Entity
    {
        public string Name { get; set; }

        public Guid? CompanyId { get; set; }

        public virtual Company Company { get; set; }
    }
}