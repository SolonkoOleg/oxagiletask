﻿using IoC.Entities.Infrastructure;
using System.Collections.Generic;

namespace IoC.Entities.Models
{
    public class Company : Entity
    {
        public string Name { get; set; }

        public virtual IEnumerable<User> Users { get; set; }
    }
}
