﻿using System;
using System.ComponentModel;

namespace IoC.ViewModels.Models
{
    public class CompanyViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Company")]
        public string Name { get; set; }
    }
}