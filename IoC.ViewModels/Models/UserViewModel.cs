﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IoC.ViewModels.Models
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public CompanyViewModel ChosedCompany { get; set; }

        public IList<CompanyViewModel> Companies { get; set; }
    }
}