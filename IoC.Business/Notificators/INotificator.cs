﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC.Business.Notificators
{
    public interface INotificator
    {
        void Notify(string message, object obj);
    }
}
