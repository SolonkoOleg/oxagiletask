﻿using Castle.Facilities.Startable;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using IoC.Data.Context;
using IoC.Data.Contracts;
using IoC.Data.Repository;

namespace IoC.Business.Helpers
{
    public class BusinessCastleInstaller : IWindsorInstaller
    {
        private readonly string connectionString;

        public BusinessCastleInstaller(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            DynamicParametersDelegate dynamicParameters = (k, p) => { p["dbContext"] = new Context(connectionString); };
            container.Register(Component.For<IUserRepository>().ImplementedBy<UserRepository>()
                .DynamicParameters(dynamicParameters).LifeStyle.PerWebRequest);
            container.Register(Component.For<ICompanyRepository>().ImplementedBy<CompanyRepository>()
                .DynamicParameters(dynamicParameters).LifeStyle.PerWebRequest);
        }
    }
}
