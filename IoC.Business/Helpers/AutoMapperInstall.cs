﻿using AutoMapper;
using IoC.Entities.Models;
using IoC.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC.Business.Helpers
{
    public static class AutoMapperInstall
    {
        public static void Install()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg => {
                cfg.CreateMap<User, UserViewModel>()
                    .ForMember(d => d.ChosedCompany, o => o.MapFrom(src => src.Company));
                cfg.CreateMap<UserViewModel, User>()
                    .ForMember(d => d.CompanyId, o => o.MapFrom(src => src.ChosedCompany.Id)); ;
                cfg.CreateMap<Company, CompanyViewModel>();
                cfg.CreateMap<CompanyViewModel, Company>();
            });
        }
    }
}
