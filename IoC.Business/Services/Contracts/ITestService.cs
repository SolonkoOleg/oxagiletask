﻿using IoC.ViewModels.Models;
using System;
using System.Collections.Generic;

namespace IoC.Business.Services.Contracts
{
    public interface ITestService
    {
        IList<CompanyViewModel> GetAllCompanies();
        CompanyViewModel GetCompanyById(Guid id);
        void AddCompany(CompanyViewModel company);

        IList<UserViewModel> GetAllUsers();
        UserViewModel GetUserById(Guid id);
        void AddUser(UserViewModel user);
        void DeleteUser(UserViewModel Id);
        void UpdateUser(UserViewModel user);
    }
}
