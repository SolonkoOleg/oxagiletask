﻿using AutoMapper;
using IoC.Business.Helpers;
using IoC.Business.Notificators;
using IoC.Business.Services.Contracts;
using IoC.Data.Contracts;
using IoC.Entities.Models;
using IoC.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IoC.Business.Services
{
    public class TestService : ITestService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICompanyRepository _companyRepository;
        private ICollection<INotificator> _notifications;

        public TestService(IUserRepository userRepository, ICompanyRepository companyRepository)
        {
            _userRepository = userRepository;
            _companyRepository = companyRepository;
            AutoMapperInstall.Install();
        }

        /*Setting up a collection of services for notifications
        (A different set of services for the different controllers)*/
        public void SetNotifications(ICollection<INotificator> notifications)
        {
            _notifications = notifications;
        }

        public IList<CompanyViewModel> GetAllCompanies()
        {
            return Mapper.Map<IList<CompanyViewModel>>(_companyRepository.GetAll().ToList());
        }

        public IList<UserViewModel> GetAllUsers()
        {
            return Mapper.Map<IList<UserViewModel>>(_userRepository.GetAll().ToList());
        }

        public IList<UserViewModel> GetUsers(Guid companyId)
        {
            return Mapper.Map<IList<UserViewModel>>(_userRepository.GetAll().Where(c => c.CompanyId == companyId).ToList());
        }

        public CompanyViewModel GetCompanyById(Guid id)
        {
            return Mapper.Map<CompanyViewModel>(_companyRepository.Get(u => u.Id.Equals(id)));
        }

        public UserViewModel GetUserById(Guid id)
        {

            return Mapper.Map<UserViewModel>(_userRepository.Get(u => u.Id.Equals(id)));
        }

        public void AddUser(UserViewModel user)
        {
            _userRepository.Add(Mapper.Map<User>(user));
            //Call services to send notification
            if (_notifications != null)
            {
                foreach(var notificator in _notifications)
                {
                    notificator.Notify("message", user);
                }
            }
        }

        public void AddCompany(CompanyViewModel company)
        {
            _companyRepository.Add(Mapper.Map<Company>(company));
        }

        public void DeleteUser(UserViewModel user)
        {
            _userRepository.Delete(user.Id);
        }

        public void UpdateUser(UserViewModel user)
        {
           _userRepository.Update(user.Id, Mapper.Map<User>(user));
        }
    }
}
