﻿using IoC.Entities.Models;

namespace IoC.Data.Contracts
{
    public interface ICompanyRepository : IRepository<Company>
    { }
}
