﻿using IoC.Entities.Models;
using System;

namespace IoC.Data.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        void Delete(Guid id);
        void Update(Guid id, User user);
    }
}
