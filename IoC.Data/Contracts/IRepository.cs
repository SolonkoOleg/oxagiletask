﻿using IoC.Entities.Infrastructure;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace IoC.Data.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        T Get(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
