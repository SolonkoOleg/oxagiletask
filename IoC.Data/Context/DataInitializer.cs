﻿using IoC.Entities.Models;
using System.Data.Entity;

namespace IoC.Data.Context
{
    internal class DataInitializer : DropCreateDatabaseAlways<Context>
    {
        protected override void Seed(Context db)
        {
            db.Companies.Add(new Company { Name = "Test1" });
            db.Companies.Add(new Company { Name = "Test2" });
            db.Companies.Add(new Company { Name = "Test3" });
            db.SaveChanges();
        }
    }
}
