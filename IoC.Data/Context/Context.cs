﻿using IoC.Entities.Models;
using System.Data.Entity;

namespace IoC.Data.Context
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }

        public Context(string connectionString) : base(connectionString) {
            Database.SetInitializer(new DataInitializer());
        }
    }
}
