﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using IoC.Data.Contracts;
using IoC.Entities.Infrastructure;

namespace IoC.Data.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : Entity
    {
        internal readonly Context.Context Context;

        public BaseRepository(Context.Context dbContext)
        {
            Context = dbContext;
        }

        public IQueryable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = Context.Set<T>();

            foreach (var navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include(navigationProperty);
            return dbQuery;
        }

        public T Get(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = Context.Set<T>();

            foreach (var navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include(navigationProperty);

            T item = dbQuery
                .FirstOrDefault(where);
            return item;
        }

        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.SaveChanges();
        }

        public void Delete(T entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
