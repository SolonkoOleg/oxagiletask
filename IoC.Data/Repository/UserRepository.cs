﻿using IoC.Data.Contracts;
using IoC.Entities.Models;
using System;
using System.Linq;

namespace IoC.Data.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(Context.Context dbContext) : base(dbContext)
        { }

        public void Delete(Guid id)
        {
            var user = Context.Users
                   .FirstOrDefault(o => o.Id == id);
            if (user == null) return;
            Context.Users.Remove(user);
            Context.SaveChanges();
        }

        public void Update(Guid id, User updUser)
        {
            var user = Context.Users
                .FirstOrDefault(c => c.Id == id);
            if (user != null)
            {
                user.Name = updUser.Name;
                user.CompanyId = updUser.CompanyId;
                Context.SaveChanges();
            }
        }
    }
}
