﻿using IoC.Data.Contracts;
using IoC.Entities.Models;
using System.Data.Entity;

namespace IoC.Data.Repository
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(Context.Context dbContext) : base(dbContext) { }
    }
}
